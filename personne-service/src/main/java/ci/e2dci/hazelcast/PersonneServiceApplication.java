package ci.e2dci.hazelcast;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;

import ci.e2dci.hazelcast.docunents.Personne;
import ci.e2dci.hazelcast.service.PersonneService;

@SpringBootApplication
@EnableCaching
@EnableKafka
public class PersonneServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonneServiceApplication.class, args);
	}

	/*
	 * @Bean CommandLineRunner start(PersonneService personneService) { return args
	 * ->{
	 * 
	 * 
	 * personneService.create0pe(new Personne(null,"Diarrassouba", "Idrissa", "dd",
	 * "yop", null)); personneService.create0pe(new Personne(null,"Traore",
	 * "Abdoulaye", "Ablo", "Riv",null)); personneService.create0pe(new
	 * Personne(null,"Fofana", "Mohamed","Fm", "yop", null));
	 * personneService.create0pe(new Personne(null,"Kouadio", "Koffi", "kk",
	 * "Abobo", null)); personneService.create0pe(new Personne(null,"Diakite",
	 * "Amy", "da", "yop", null));
	 * 
	 * 
	 * personneService.findAll().stream().forEach(p ->
	 * personneService.findById(p.getId())); };
	 * 
	 * }
	 */

}
