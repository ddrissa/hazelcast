package ci.e2dci.hazelcast.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.e2dci.hazelcast.docunents.Personne;

public interface PersonneRepository extends MongoRepository<Personne, String> {
 List<Personne> findByNom(String nom);
 List<Personne> findByPrenom(String prenom);
 List<Personne> findByNomOrPrenom(String nom, String prenom);
 
}
