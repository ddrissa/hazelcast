package ci.e2dci.hazelcast.confg;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.ManagementCenterConfig;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;

import ci.e2dci.hazelcast.docunents.ContactPerso;
import ci.e2dci.hazelcast.model.Proprietaire;

@Configuration
public class HazelCastConfiguration {
	// Configuration Hazelcast
	@Bean
	public Config hazelCastConfi() {
		Config config = new Config();
		ManagementCenterConfig centerConfig = new ManagementCenterConfig();
		centerConfig.setEnabled(true);
		centerConfig.setUrl("http://localhost:8080/hazelcast-mancenter/");
		config.setManagementCenterConfig(centerConfig);
		config.setInstanceName("hazelcast-instance")
				.addMapConfig(new MapConfig().setName("personnes")
						.setMaxSizeConfig(new MaxSizeConfig(500, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
						.setEvictionPolicy(EvictionPolicy.LRU).setTimeToLiveSeconds(-1));
		return config;
	}

	// ################# KAFKA #################################
	// ****************Configuration Kafka producer**************
	@Bean
	public ProducerFactory<String, Proprietaire> producerFactory() {
		Map<String, Object> config = new HashMap<>();
		config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

		return new DefaultKafkaProducerFactory<>(config);

	}

	@Bean
	public KafkaTemplate<String, Proprietaire> kafkaTemplate() {
		return new KafkaTemplate<>(producerFactory());
	}
	
	// ################# KAFKA #################################
	// ****************Configuration Kafka producer 2**************
	@Bean
	public ProducerFactory<String, ContactPerso> producerContactFactory() {
		Map<String, Object> config = new HashMap<>();
		config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
		
		return new DefaultKafkaProducerFactory<>(config);
		
	}
	
	@Bean
	public KafkaTemplate<String, ContactPerso> kafkaTemplateContact() {
		return new KafkaTemplate<>(producerContactFactory());
	}

	// Configuration Kafka consommeer
	// Avec objet a desserialise

	/*
	 * @Bean public ConsumerFactory<String, Contact> consumerFactory() { Map<String,
	 * Object> config = new HashMap<>();
	 * 
	 * config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
	 * config.put(ConsumerConfig.GROUP_ID_CONFIG, "groupContacts");
	 * config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
	 * StringDeserializer.class);
	 * config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
	 * JsonDeserializer.class);
	 * 
	 * return new DefaultKafkaConsumerFactory<>(config, new StringDeserializer(),
	 * new JsonDeserializer<>(Contact.class)); }
	 * 
	 * @Bean public ConcurrentKafkaListenerContainerFactory<String, Contact>
	 * kafkaListenerContainerFactory() {
	 * 
	 * ConcurrentKafkaListenerContainerFactory<String, Contact>
	 * concurrentKafkaListenerContainerFactory = new
	 * ConcurrentKafkaListenerContainerFactory<>();
	 * concurrentKafkaListenerContainerFactory.setConsumerFactory(consumerFactory())
	 * ;
	 * 
	 * return concurrentKafkaListenerContainerFactory;
	 * 
	 * }
	 */

	// ***********************Sans deserisaliser objet*************************

	@Bean
	public ConsumerFactory<String, String> consumerFactory() {
		Map<String, Object> config = new HashMap<>();

		config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		config.put(ConsumerConfig.GROUP_ID_CONFIG, "groupContacts");
		config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

		return new DefaultKafkaConsumerFactory<>(config, new StringDeserializer(), new StringDeserializer());
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {

		ConcurrentKafkaListenerContainerFactory<String, String> concurrentKafkaListenerContainerFactory = new ConcurrentKafkaListenerContainerFactory<>();
		concurrentKafkaListenerContainerFactory.setConsumerFactory(consumerFactory());

		return concurrentKafkaListenerContainerFactory;

	}
	
	@Bean
	public ObjectMapper jsonMapper(){
		return new ObjectMapper();
	}

}
