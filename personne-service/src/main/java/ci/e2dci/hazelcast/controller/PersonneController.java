package ci.e2dci.hazelcast.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ci.e2dci.hazelcast.docunents.ContactPerso;
import ci.e2dci.hazelcast.docunents.Personne;
import ci.e2dci.hazelcast.service.PersonneService;

@RestController
public class PersonneController {
	
	private PersonneService personneService;

	public PersonneController(PersonneService personneService) {
		super();
		this.personneService = personneService;
	}
	
	
	@PostMapping("/personnes")
	public Personne create( @RequestBody Personne personne) {
		return personneService.create0pe(personne);
	}
	
	
	@GetMapping("/personnes")
	public List<Personne> listPersonne() {
		return personneService.findAll();
	}
	
	
	@GetMapping("/personnes/{id}")
	public Personne findById(@PathVariable String id) {
		return personneService.findById(id);
	}
	
	@PutMapping("/personnes/{id}")
	public Personne update(@PathVariable String id, @RequestBody Personne personne) {
		Personne p= personneService.findById(id);
		if (p!=null) {
			 p=personneService.update(id, p);
		}
		
		return p;
	}
	
	@PatchMapping("/personnes/{id}")
	public boolean emleverContact(@PathVariable String id, @RequestBody ContactPerso cp) {
		Personne p= personneService.findById(id);
		boolean result=false;
		if (Objects.nonNull(p)) {
			result=personneService.enleverContact(id, cp);
		}
		
		return result;
	}
	

	@DeleteMapping("/personnes/{id}")
	public boolean delete(@PathVariable String id) {
		return personneService.delete(id);
	}

}
