package ci.e2dci.hazelcast.docunents;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Document
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(of = { "id", "nom", "prenom", "surnom", "domicile" })
@EqualsAndHashCode(of = { "id", "nom", "prenom" })
public class Personne implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String nom;
	private String prenom;
	private String surnom;
	private String domicile;
	Set<ContactPerso> contacts = new HashSet<>();

	public Personne(String id, String nom, String prenom, String surnom, String domicile) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.surnom = surnom;
		this.domicile = domicile;
	}

}
