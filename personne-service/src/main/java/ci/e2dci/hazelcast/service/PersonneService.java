package ci.e2dci.hazelcast.service;

import java.util.List;
import java.util.Set;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import ci.e2dci.hazelcast.docunents.ContactPerso;
import ci.e2dci.hazelcast.docunents.Personne;
import ci.e2dci.hazelcast.model.Proprietaire;
import ci.e2dci.hazelcast.repository.PersonneRepository;

@Service
public class PersonneService {

	private PersonneRepository personneRepository;
	private KafkaTemplate<String, Proprietaire> kafkaTemplate;
	private KafkaTemplate<String, ContactPerso> contactkafkaTemplate;

	
	


	public PersonneService(PersonneRepository personneRepository, KafkaTemplate<String, Proprietaire> kafkaTemplate,
			KafkaTemplate<String, ContactPerso> contactkafkaTemplate) {
		super();
		this.personneRepository = personneRepository;
		this.kafkaTemplate = kafkaTemplate;
		this.contactkafkaTemplate = contactkafkaTemplate;
	}

	//@CachePut(value = "personnes", key = "#id")
	public Personne  create0pe(Personne personne) {
		
		Personne p =personneRepository.save(personne);
		Proprietaire pr = new Proprietaire(p.getId(), p.getNom(), p.getPrenom(), p.getSurnom(), p.getDomicile());
		kafkaTemplate.send("persoTopic", pr);
		System.out.println("#############################");
		System.out.println("Preprietaire ajoute au topic:"+pr.toString());
		return p;
	}
	
	//@Cacheable(cacheNames = "personnes")
	public List<Personne> findAll() {
		System.out.println("J'utilise la methode findAll");
		return personneRepository.findAll();
		
	}
	
	@Cacheable(value = "personnes", key = "#id", unless = "#result==null")
	public Personne findById(String id) {
		System.out.println("J'utilise la methode findById");
		return personneRepository.findById(id).get();
		
	}

	//Utiliser pour kafka
	@CacheEvict(value = "personnes", key ="#id" )
	public Personne updateContact(String id, Personne p) {
		//Personne p = personneRepository.findById(id).get();
		Personne p2 =personneRepository.save(p);
		System.out.println("********************* perso maj *************");
		System.out.println(p2.toString());
		System.out.println("######### les contacts sont ########");
		System.out.println(p2.getContacts());
		
		return p2;
		
	}
	
	public boolean enleverContact(String id, ContactPerso cp) {
		System.out.println("Entrer enlever");
		Personne p = personneRepository.findById(id).get();
		Set<ContactPerso> contacts= p.getContacts();
		System.out.println("Contact ajout au topic:"+contacts);
		boolean rm=contacts.remove(cp);
		System.out.println("Resultat enlever "+ rm);
		p.setContacts(contacts);
		p =personneRepository.save(p);
		if (rm) {
			System.out.println("Dans if");
			contactkafkaTemplate.send("contactPersoTopic", cp);
			System.out.println("#############################");
			System.out.println("Contact ajout au topic:"+cp.toString());
		}
		
		
		return rm;
		
	}
	@CacheEvict(value = "personnes", key ="#id" )
	public boolean delete(String id) {
		Personne per = personneRepository.findById(id).get();
		personneRepository.deleteById(id);
		Proprietaire pr = new Proprietaire(per.getId(), per.getNom(), per.getPrenom(), per.getSurnom(), per.getDomicile());
		kafkaTemplate.send("persoSuppTopic", pr);
		System.out.println("#############################");
		System.out.println("Preprietaire ajoute au topic:"+pr.toString());
		return true;
	}
	
	
	public Personne update(String id,Personne p) {
		Personne per= new Personne(id, p.getNom(), p.getPrenom(), p.getSurnom(), p.getDomicile());
		
		 per =personneRepository.save(per);
		Proprietaire pr = new Proprietaire(per.getId(), per.getNom(), per.getPrenom(), per.getSurnom(), per.getDomicile());
		kafkaTemplate.send("persoTopic", pr);
		System.out.println("#############################");
		System.out.println("Preprietaire ajoute au topic:"+pr.toString());
		return per;
		
	}
}
