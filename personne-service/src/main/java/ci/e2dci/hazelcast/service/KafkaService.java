package ci.e2dci.hazelcast.service;

import java.util.Set;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ci.e2dci.hazelcast.docunents.ContactPerso;
import ci.e2dci.hazelcast.docunents.Personne;
import ci.e2dci.hazelcast.model.Contact;

@Service
public class KafkaService {

	private PersonneService personneService;

	private ObjectMapper jsonMapper;

	

	public KafkaService(PersonneService personneService, ObjectMapper jsonMapper) {
		super();
		this.personneService = personneService;
		this.jsonMapper = jsonMapper;
	}



	@KafkaListener(topics = "contactTopic", groupId = "groupContacts")
	public void getProprietaire(String stingContact) {
		
		try {
			Contact c = jsonMapper.readValue(stingContact, Contact.class);
			ContactPerso contact= new ContactPerso(c.getId(), c.getNumero(), c.getLibelle(), c.getOperateur());
			Personne p= personneService.findById(c.getProprietaire().getId());
			Set<ContactPerso> contacts= p.getContacts();
			contacts.add(contact);
			p.setContacts(contacts);
			System.out.println("*************** Personne du contact distant trouve est: ****************");
			System.out.println(p.toString());
			System.out.println("############ Ses contacts sont: #########");
			System.out.println(p.getContacts());
			personneService.updateContact(p.getId(), p);
		} catch (JsonProcessingException e) {
			System.out.println("######### erreur trouve ####################");
			System.out.println(e.getMessage());
		}

	}

}
