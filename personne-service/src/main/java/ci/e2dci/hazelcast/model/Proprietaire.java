package ci.e2dci.hazelcast.model;


import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@ToString @EqualsAndHashCode
public class Proprietaire {

	private String id;
	private String nom;
	private String prenom;
	private String surnom;
	private String domicile;
}
