package ci.e2dci.hazelcast.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode(of = { "id", "numero" })
public class Contact implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private String numero;
	private String libelle;
	private String operateur;
	private Proprietaire proprietaire;

	
	
	public Contact(String numero, String libelle, String operateur) {
		super();
		this.numero = numero;
		this.libelle = libelle;
		this.operateur = operateur;
	}



	public Contact() {
		super();
		// TODO Auto-generated constructor stub
	}

}
