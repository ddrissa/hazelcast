package ci.e2dci.hazelcast.documents;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Document
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Contact implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private String numero;
	private String libelle;
	private String operateur;
	private Proprietaire proprietaire;

	public Contact(String numero, String libelle, String operateur, Proprietaire proprietaire) {
		super();
		this.numero = numero;
		this.libelle = libelle;
		this.operateur = operateur;
		this.proprietaire = proprietaire;
	}

}
