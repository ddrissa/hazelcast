package ci.e2dci.hazelcast.documents;


import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@ToString @EqualsAndHashCode
@Document
public class Proprietaire implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String nom;
	private String prenom;
	private String surnom;
	private String domicile;
}
