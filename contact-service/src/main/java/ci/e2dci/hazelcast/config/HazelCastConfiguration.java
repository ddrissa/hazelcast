package ci.e2dci.hazelcast.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.ManagementCenterConfig;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;

import ci.e2dci.hazelcast.documents.Contact;

@Configuration
public class HazelCastConfiguration {

	// ################ HAZELCAST ###########
	
	@Bean
	public Config hazelCastConfi() {
		Config config = new Config();

		ManagementCenterConfig centerConfig = new ManagementCenterConfig();
		centerConfig.setEnabled(true);
		centerConfig.setUrl("http://localhost:8080/hazelcast-mancenter/");
		config.setManagementCenterConfig(centerConfig);

		config.setInstanceName("hazelcast-instance")
				.addMapConfig(new MapConfig().setName("contacts")
						.setMaxSizeConfig(new MaxSizeConfig(500, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
						.setEvictionPolicy(EvictionPolicy.LRU).setTimeToLiveSeconds(-1));
		return config;
	}

	/*
	 * @Bean public ClientConfig clientConfig() { ClientConfig clientConfig= new
	 * ClientConfig(); clientConfig.setInstanceName("hazelcastClient-instance");
	 * 
	 * return clientConfig;
	 * 
	 * }
	 * 
	 * @Bean public HazelcastInstance client() { return
	 * HazelcastClient.newHazelcastClient(clientConfig()); }
	 */

	// ############### KAFKA #############################
	// *************producer kafka configuration***********************
	@Bean
	public ProducerFactory<String, Contact> producerFactory() {
		Map<String, Object> config = new HashMap<>();

		config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

		return new DefaultKafkaProducerFactory<String, Contact>(config);
	}

	@Bean
	public KafkaTemplate<String, Contact> kafkaTemplate() {
		return new KafkaTemplate<>(producerFactory());
	}

	// ********************** Configuration Kafka consommeer****************
	// Avec objet a desserialise

	/*
	 * @Bean public ConsumerFactory<String, Proprietaire> consumerFactory() {
	 * 
	 * Map<String, Object> config = new HashMap<>();
	 * 
	 * config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
	 * config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
	 * StringDeserializer.class);
	 * config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
	 * JsonDeserializer.class); config.put(ConsumerConfig.GROUP_ID_CONFIG,
	 * "groupPers");
	 * 
	 * return new DefaultKafkaConsumerFactory<String, Proprietaire>(config, new
	 * StringDeserializer(), new JsonDeserializer<>(Proprietaire.class)); }
	 * 
	 * 
	 * @Bean public ConcurrentKafkaListenerContainerFactory<String, Proprietaire>
	 * concurrentKafkaListenerContainerFactory() {
	 * ConcurrentKafkaListenerContainerFactory<String, Proprietaire>
	 * concurrentKafkaListenerContainerFactory = new
	 * ConcurrentKafkaListenerContainerFactory<>();
	 * concurrentKafkaListenerContainerFactory.setConsumerFactory(consumerFactory())
	 * ; return concurrentKafkaListenerContainerFactory; }
	 */

	// ***********************Sans deserisaliser objet*************************
	
	@Bean
	public ConsumerFactory<String, String> consumerFactory() {

		Map<String, Object> config = new HashMap<>();

		config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		config.put(ConsumerConfig.GROUP_ID_CONFIG, "groupPerso");
		
		return new DefaultKafkaConsumerFactory<String, String>(config, new StringDeserializer(), new StringDeserializer());
	}
	
	
	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, String> concurrentkafkaListenerContainerFactory = new ConcurrentKafkaListenerContainerFactory<>();
		concurrentkafkaListenerContainerFactory.setConsumerFactory(consumerFactory());
		return concurrentkafkaListenerContainerFactory;
	}
	
	@Bean
	public ObjectMapper jsonMapper(){
		return new ObjectMapper();
	}

	
}
