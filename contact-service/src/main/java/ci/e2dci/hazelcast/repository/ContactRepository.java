package ci.e2dci.hazelcast.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.e2dci.hazelcast.documents.Contact;
import ci.e2dci.hazelcast.documents.Proprietaire;

public interface ContactRepository extends MongoRepository<Contact, String> {
	
	List<Contact> findByLibelle(String libelle);
	List<Contact> findByOperateur(String operateur);
	List<Contact> findByProprietaire(Proprietaire proprietaire);
	
	
	

}
