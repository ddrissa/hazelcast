package ci.e2dci.hazelcast.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.e2dci.hazelcast.documents.Proprietaire;

public interface ProprietaireRepository extends MongoRepository<Proprietaire, String> {

}
