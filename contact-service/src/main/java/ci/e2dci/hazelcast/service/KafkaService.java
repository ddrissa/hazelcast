package ci.e2dci.hazelcast.service;

import java.util.List;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ci.e2dci.hazelcast.documents.Contact;
import ci.e2dci.hazelcast.documents.Proprietaire;
import ci.e2dci.hazelcast.model.ContactPerso;

@Service
public class KafkaService {

	private ProprietaireService proprietaireService;
	private ContactService contactService;

	private ObjectMapper jsonMapper;

	

	public KafkaService(ProprietaireService proprietaireService, ContactService contactService,
			ObjectMapper jsonMapper) {
		super();
		this.proprietaireService = proprietaireService;
		this.contactService = contactService;
		this.jsonMapper = jsonMapper;
	}


	@KafkaListener(topics = "persoTopic", groupId = "groupPerso")
	public void getProprietaire(String stingPropretaire) {
		
		try {
			Proprietaire proprietaire = jsonMapper.readValue(stingPropretaire, Proprietaire.class);
			
			proprietaire= proprietaireService.create(proprietaire);
			System.out.println("*************** Ajout un proprietaire distant ****************");
			System.out.println(proprietaire.toString());
		} catch (JsonProcessingException e) {
			System.out.println("######### erreur trouve ####################");
			System.out.println(e.getMessage());
		}

	}
	
	
	@KafkaListener(topics = "persoSuppTopic", groupId = "groupPerso")
	public void deleteProprietaire(String stingPropretaire) {
		
		try {
			Proprietaire proprietaire = jsonMapper.readValue(stingPropretaire, Proprietaire.class);
			List<Contact> contacts= contactService.findByProprietaire(proprietaire); 
			System.out.println("***********les contacts trouves **********");
			System.out.println(contacts);
			 proprietaireService.deleteById(proprietaire.getId());
			 System.out.println("***********"+proprietaire.toString()+"supprimer");
			contactService.supprimeAllProp(contacts);
			System.out.println("*********** tous ces contacts supprimer");
		} catch (JsonProcessingException e) {
			System.out.println("######### Causes des erreurs ####################");
			System.out.println(e.getMessage());
		}
		
	}
	
	@KafkaListener(topics = "contactPersoTopic", groupId = "groupPerso")
	public void deleteContactPerso(String stingContacPerso) {
		
		try {
			ContactPerso cp = jsonMapper.readValue(stingContacPerso, ContactPerso.class);
			contactService.supprime(cp.getId());
			System.out.println("***********le contact trouve **********");
			System.out.println(cp);
			
		} catch (JsonProcessingException e) {
			System.out.println("######### Causes des erreurs ####################");
			System.out.println(e.getMessage());
		}
		
	}

}
