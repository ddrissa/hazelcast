package ci.e2dci.hazelcast.service;

import org.springframework.stereotype.Service;

import ci.e2dci.hazelcast.documents.Proprietaire;
import ci.e2dci.hazelcast.repository.ProprietaireRepository;

@Service
public class ProprietaireService {
private ProprietaireRepository proprietaireRepository;

public ProprietaireService(ProprietaireRepository proprietaireRepository) {
	super();
	this.proprietaireRepository = proprietaireRepository;
}


public Proprietaire	 create(Proprietaire p) {
	return proprietaireRepository.save(p);
	
}

public Proprietaire  getById(String id) {
	return proprietaireRepository.findById(id).get();
}
public void  deleteById(String id) {
	 proprietaireRepository.deleteById(id);
}
}
