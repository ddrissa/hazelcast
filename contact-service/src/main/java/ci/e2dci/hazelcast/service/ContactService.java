package ci.e2dci.hazelcast.service;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import ci.e2dci.hazelcast.documents.Contact;
import ci.e2dci.hazelcast.documents.Proprietaire;
import ci.e2dci.hazelcast.repository.ContactRepository;

@Service
public class ContactService {

	private ContactRepository contactRepository;
	//private ProprietaireService proprietaireService;
	private KafkaTemplate<String, Contact> kafkaTemplate;
	//private WebClient client;

	public ContactService(ContactRepository contactRepository, KafkaTemplate<String, Contact> kafkaTemplate) {
		super();
		this.contactRepository = contactRepository;
		this.kafkaTemplate = kafkaTemplate;
	}


	
	
	//@Cacheable(cacheNames = {"contacts"})
	public List<Contact> findAll() {
		System.out.println("Appel methode findAll");
		return contactRepository.findAll();
		
	}
	
	

	//@CachePut(value = "contacts", key = "#id")
	public Contact create(Contact contact) {
		Contact c=contactRepository.save(contact);
		kafkaTemplate.send("contactTopic", c);
		System.out.println("#############################");
		System.out.println("Contact ajoute au contactTopic:"+c.toString());
		return c;
	}
	
	@Cacheable(value = "contacts", key = "#id", unless = "#result==null")
	public Contact getByid(String id) {
		System.out.println("Appel methode findById");
		return contactRepository.findById(id).get();
		
	}
	
	//@Cacheable(value = "personnes", key = "#id", unless = "#result==null")
	/*
	 * public Mono<Personne> getPersonneById(String id) {
	 * System.out.println("Appel distance de personne");
	 * 
	 * return
	 * client.get().uri("/personnes/"+id).retrieve().bodyToMono(Personne.class);
	 * 
	 * }
	 */
	
	@CacheEvict(value = "contacts", key = "#id.")
	public Contact  update(String id,Contact contact) {
		Contact c= contactRepository.findById(id).get();
		Contact c2= new Contact(c.getId(), c.getNumero(), c.getLibelle(), c.getOperateur(), c.getProprietaire());
		
		 c2=contactRepository.save(contact);
		kafkaTemplate.send("contactTopic", c2);
		System.out.println("#############################");
		System.out.println("Contact ajoute au contactTopic:"+c2.toString());
		return c2;
	}
	
	// Pour kafka
	public List<Contact> findByProprietaire(Proprietaire proprietaire) {
		
		return contactRepository.findByProprietaire(proprietaire);
		
	}
	
	@CacheEvict(value = "contacts", allEntries = true)
	public void supprimeAllProp(List<Contact> contacts) {
		contactRepository.deleteAll(contacts);
	}
	
	
	@CacheEvict(value = "contacts", key = "#id.")
	public void supprime(String id) {
		contactRepository.deleteById(id);
	}
}
