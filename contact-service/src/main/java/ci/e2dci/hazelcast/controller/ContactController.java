package ci.e2dci.hazelcast.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ci.e2dci.hazelcast.documents.Contact;
import ci.e2dci.hazelcast.documents.Proprietaire;
import ci.e2dci.hazelcast.model.ContactRemonte;
import ci.e2dci.hazelcast.service.ContactService;
import ci.e2dci.hazelcast.service.ProprietaireService;

@RestController
public class ContactController {
	private ContactService contactService;
	private ProprietaireService proprietaireService;

	
	
	public ContactController(ContactService contactService, ProprietaireService proprietaireService) {
		super();
		this.contactService = contactService;
		this.proprietaireService = proprietaireService;
	}

	@PostMapping("/contacts")
	public Contact create(@RequestBody ContactRemonte remonte) {
		System.out.println("Entrer methoode");
		Proprietaire pro = proprietaireService.getById(remonte.getPersId());
		System.out.println("appres proprietaire ");
		//Personne p= new Personne(pers.getId(), pers.getNom(), pers.getPrenom(), pers.getSurnom(), pers.getDomicile());
		Contact c = new Contact(remonte.getNumero(), remonte.getLibelle(), remonte.getOperateur(), pro);
		return contactService.create(c);
	}
	
	@GetMapping("/contacts")
	public List<Contact> getAll() {
	
		return contactService.findAll();
	}
	

	@GetMapping("/contacts/{id}")
	public Contact getById(@PathVariable String id) {
		
		return contactService.getByid(id);
	}
	
	@PatchMapping("/contacts/{id}")
	public Contact update(@PathVariable String id, @RequestBody Contact contact) {
		Contact c=contactService.getByid(id);
		
		return contactService.update(c.getId(), contact);
	}
	
	@DeleteMapping("/contacts/{id}")
	public Contact supprimr(@PathVariable String id) {
		
		return contactService.getByid(id);
	}

}
