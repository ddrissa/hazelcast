package ci.e2dci.hazelcast.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContactRemonte {

	private String numero;
	private String libelle;
	private String operateur;
	private String persId;
}
