package ci.kossovogateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {
	
	@Bean
	RouteLocator gatewayRoute(RouteLocatorBuilder builder) {
		return builder.routes()
		.route(r -> r.path("/personnes/**").uri("lb://personne-service").id("route-1"))
		.route(r -> r.path("/contacts/**").uri("lb://contact-service").id("route-2"))
		.build();
		
	}

}
